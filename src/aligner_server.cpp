#include <ros/ros.h>
#include <sensor_msgs/PointCloud2.h>
#include <geometry_msgs/TransformStamped.h>
#include <tf2_ros/static_transform_broadcaster.h>

#include <clipper/clipper.h>
#include <clipper/invariants/builtins.h>

#include <Eigen/Geometry>
#include <tf2_eigen/tf2_eigen.h>

#include <sensor_msgs/point_cloud_conversion.h>
#include <pcl_conversions/pcl_conversions.h>
#include <pcl_ros/transforms.h>
#include <pcl/point_cloud.h>
#include <pcl/point_types.h>

#include <visualization_msgs/MarkerArray.h>

#include <sensor_msgs/point_cloud2_iterator.h>

#include <arl_color_tools/color.h>

using Landmarks = sensor_msgs::PointCloud2;
using Clipper = clipper::CLIPPER<clipper::invariants::EuclideanDistance>;

// ======================================================================
// Align two landmark sets using CLIPPER
// ======================================================================

Eigen::Affine3d align_maps(Clipper& clipper, const Landmarks& l0, const Landmarks& l1)
{
  // nothing to do if maps aren't big enough to estimate (R,t)
  if (l0.width < 3 || l1.width < 3) {
    ROS_WARN_STREAM("Landmark sets too small to align...");
    return Eigen::Affine3d::Identity();
  }

  pcl::PointCloud<pcl::PointXYZ> l0_cloud, l1_cloud;
  pcl::fromROSMsg(l0, l0_cloud);
  pcl::fromROSMsg(l1, l1_cloud);

  // Assuming 3-d points (could probably be inferred from point cloud schema)
  Eigen::MatrixXd model = l0_cloud.getMatrixXfMap().topRows(3).cast<double>();
  Eigen::MatrixXd data  = l1_cloud.getMatrixXfMap().topRows(3).cast<double>();

  // identify data assocation
  const auto t1 = std::chrono::high_resolution_clock::now();
  auto Ain_ = clipper.findCorrespondences(model, data);
  const auto t2 = std::chrono::high_resolution_clock::now();

  const auto duration = std::chrono::duration_cast<std::chrono::nanoseconds>(t2 - t1);
  const double elapsed_ms = static_cast<double>(duration.count()) * 1e-6;

  const int m = model.cols() * data.cols(); // assumes all-to-all hypothesis
  ROS_INFO_STREAM("CLIPPER found " << Ain_.rows() << " out of " << m
    << " associations in " << std::round(elapsed_ms) << " ms");

  // we needed at least N correspondences to estimate (R,t)
  if (Ain_.rows() < 3) {
    ROS_WARN_STREAM("Not enough correspondences to compute transform...");
    return Eigen::Affine3d::Identity();
  }

  // rearrange data so that cols correspRond
  Eigen::Matrix3Xd p = Eigen::Matrix3Xd(3, Ain_.rows());
  Eigen::Matrix3Xd q = Eigen::Matrix3Xd(3, Ain_.rows());
  for (size_t i=0; i<Ain_.rows(); ++i) {
    p.col(i) = model.col(Ain_(i,0));
    q.col(i) = data.col(Ain_(i,1));
  }
  // ROS_WARN_STREAM("p: " << p);
  // ROS_WARN_STREAM("q: " << q);

  // find transformation that registers q onto p
  Eigen::Matrix4d T_pq = Eigen::umeyama(q, p, false);
  Eigen::Affine3d T_out;
  T_out.translation() = T_pq.block<3,1>(0,3);
  T_out.linear() = T_pq.block<3,3>(0,0);
  return T_out;
}

// ======================================================================
// Information for one landmark source
// ======================================================================

struct Source
{
  EIGEN_MAKE_ALIGNED_OPERATOR_NEW
  Landmarks landmarks;
  Eigen::Affine3d transform;
  visualization_msgs::Marker marker;
};

// ======================================================================

int main(int argc, char **argv)
{
  ros::init(argc, argv, "aligner_server");
  ros::NodeHandle nh, pnh("~");

  // The name of our "world" frame
  auto world_frame_id = pnh.param<std::string>("world_frame_id", "world");
  // How big to display the landmark sets
  auto landmark_display_size = pnh.param<double>("landmark_display_size", 1.0);

  // The current "primary" landmark set (largest)
  std::string primary;
  // The current root landmark set (first)
  std::string root;
  // Storage of unique point clouds from each source
  std::map<std::string, Source, std::less<std::string>,
    Eigen::aligned_allocator<std::pair<const std::string, Source> > > data;

  // Instantiate CLIPPER
  clipper::Params params;
  clipper::invariants::EuclideanDistance::Params iparams;
  iparams.sigma = pnh.param<double>("clipper_sigma", 3);
  iparams.epsilon = pnh.param<double>("clipper_epsilon", 6);
  iparams.mindist = pnh.param<double>("clipper_mindist", 2);
  clipper::CLIPPER<clipper::invariants::EuclideanDistance> clipper(params, iparams);

  // Transform broadcaster
  tf2_ros::StaticTransformBroadcaster stfb;

  // ------------------------------------------------------------
  // Create landmark visualizations
  // ------------------------------------------------------------
  auto update_markers = [landmark_display_size, &world_frame_id](const std::string& source, Source& data)
  {
    ROS_INFO_STREAM("updating markers for " << source << " with " << data.landmarks.width << " points");
    // data.marker.header.frame_id = world_frame_id;
    // data.marker.header.stamp    = data.landmarks.header.stamp;
    data.marker.header = data.landmarks.header;
    data.marker.ns     = source;
    data.marker.id     = 0;
    data.marker.type   = visualization_msgs::Marker::SPHERE_LIST;
    data.marker.action = visualization_msgs::Marker::MODIFY;
    // data.marker.pose   = tf2::toMsg(data.transform);
    data.marker.pose.orientation.w = 1.0;
    data.marker.scale.x = landmark_display_size;
    data.marker.scale.y = landmark_display_size;
    data.marker.scale.z = landmark_display_size;
    if (data.marker.color.a == 0.0)
      data.marker.color = getRandomColor();
    data.marker.frame_locked = true;
    data.marker.points.resize(data.landmarks.width);
    int count = 0;
    for (sensor_msgs::PointCloud2ConstIterator<float> it(data.landmarks, "x"); it != it.end(); ++it)
    {
      data.marker.points[count].x = it[0];
      data.marker.points[count].y = it[1];
      data.marker.points[count].z = it[2];
      ++count;
    }
  };

  // ------------------------------------------------------------
  // Publisher for landmark visualizations
  // ------------------------------------------------------------
  auto pub = nh.advertise<visualization_msgs::MarkerArray>("landmarks", 10,
      // Send latest visualizations if we've missed them
      [&](const ros::SingleSubscriberPublisher& spub)
      {
        visualization_msgs::MarkerArray out;
        for (auto& kv : data)
          out.markers.push_back(kv.second.marker);
        spub.publish(out);
      });

  // ------------------------------------------------------------
  // Take landmark inputs, looking at the publisher information to disambiguate
  // ------------------------------------------------------------
  auto sub = nh.subscribe<Landmarks>("inputs", 10,
      // Must explicitly declare callback type for MessageEvent functions
      boost::function<void(const ros::MessageEvent<Landmarks const>&)>(
        [&](const ros::MessageEvent<Landmarks const>& event)
        {
          auto source = event.getPublisherName();
          auto msg = event.getMessage();
  
          // Do some sanity checking on the input frame id
          if (msg->header.frame_id.empty())
          {
            ROS_ERROR_STREAM("Empty frame id from callerid " << source << ", ignoring...");
            return;
          }
          auto unique_frame = source.substr(1) + "/" + msg->header.frame_id;
          /*
          for (auto& kv : data)
          {
            if ((source != kv.first) &&
                (msg->header.frame_id == kv.second.landmarks.header.frame_id))
            {
              ROS_ERROR_STREAM("Duplicate frame id from callerid " << source << " -- "
                  << msg->header.frame_id << " is already being used by " << kv.first);
              return;
            }
          }
          if ((data.count(source) > 0) &&
              (msg->header.frame_id != data[source].landmarks.header.frame_id))
          {
            ROS_ERROR_STREAM("Different frame id from callerid " << source << " -- "
                << "previous value was " << data[source].landmarks.header.frame_id
                << " and new value is " << msg->header.frame_id);
            return;
          }
          */
  
          // Store the most current information from this source
          if (data.count(source) == 0)
            data[source] = {
              *msg,
              Eigen::Affine3d::Identity()
            };
          else
          {
            if (msg->header.stamp > data[source].landmarks.header.stamp)
              data[source].landmarks = *msg;
          }
          data[source].landmarks.header.frame_id = unique_frame;
  
          // The first input will become the tf tree root frame
          if (primary.empty()) root = source;
  
          // Should this be the new primary landmark set?
          if (primary.empty() || msg->width > data[primary].landmarks.width)
          {
            primary = source;
            // Recompute alignment with new primary
            for (auto& kv : data)
            {
              kv.second.transform = kv.first == primary ?
                Eigen::Affine3d::Identity() :
                align_maps(clipper, *msg, kv.second.landmarks);
            }
          }
          else
          {
            // Recompute alignment with current primary
            data[source].transform = source == primary ?
              Eigen::Affine3d::Identity() :
              align_maps(clipper, data[primary].landmarks, *msg);
          }
  
          // Now build our current transform set to the root
          std::vector<geometry_msgs::TransformStamped> tfs;
          // The primary<-root transform
          Eigen::Affine3d t_root_from_primary = data[root].transform.inverse();
          for (auto& kv : data)
          {
            Eigen::Affine3d t = t_root_from_primary * kv.second.transform;
            auto tf = tf2::eigenToTransform(t);
            tf.header.stamp = ros::Time();
            tf.header.frame_id = world_frame_id;
            tf.child_frame_id = kv.second.landmarks.header.frame_id;
            tfs.push_back(tf);
          }
  
          // Publish out new transforms as a static transform set
          stfb.sendTransform(tfs);

          // Update our visualization information and publish this new marker set
          visualization_msgs::MarkerArray markers;
          update_markers(source, data[source]);
          markers.markers.push_back(data[source].marker);
          pub.publish(markers);
        })
  );

  // Generate tf information
  ros::spin();
  return 0;
}
