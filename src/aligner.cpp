/**
 * @file aligner.cpp
 * @brief CLIPPER object-based alignment
 * @author Kaveh Fathian <kavehfathian@gmail.com>
 * @author Parker Lusk <plusk@mit.edu>
 * @date October 2021
 */

#include "aligner/aligner.h"

// #include <functional>
#include <chrono>
#include <cmath>

#include <sensor_msgs/point_cloud_conversion.h>
#include <visualization_msgs/Marker.h>

#include <pcl_conversions/pcl_conversions.h>
#include <pcl_ros/transforms.h>
#include <pcl/point_cloud.h>
#include <pcl/point_types.h>


namespace clipperlcd {

Aligner::Aligner(const ros::NodeHandle& nh, const ros::NodeHandle& nhp)
: nh_(nh), nhp_(nhp)
{
	if(!nhp_.getParam("robots", robots)) {
		ROS_FATAL("Must specify array of robot names in param `robots`");
		ros::shutdown();
	}
	num_robots = robots.size();
  nhp_.getParam("trajectory_topics", trajectory_topics_);
  // get number of robots
  if (num_robots==0) return;
  else {
    for (size_t i=0; i<num_robots; ++i) 
    {
      ROS_INFO_STREAM("robot name: " << robots[i]);
      ROS_INFO_STREAM("traj name: " << trajectory_topics_[i]);
    }
  }
  
  // clipper params
  nhp_.getParam("clipper_sigma", clipper_sigma);
  ROS_INFO_STREAM("clipper_sigma: " << clipper_sigma);
  nhp_.getParam("clipper_epsilon", clipper_epsilon);
  ROS_INFO_STREAM("clipper_epsilon: " << clipper_epsilon);
  nhp_.getParam("clipper_mindist", clipper_mindist);
  ROS_INFO_STREAM("clipper_mindist: " << clipper_mindist);

  nhp_.param("map_frame_id", map_frame_id_, std::string("map"));
  nhp_.param("world_frame_id", world_frame_id_, std::string("world"));
  

  // initializers
  sub_r_landmarks_.resize(num_robots);
  sub_r_traj_.resize(num_robots);
  pubviz_r_frame_.resize(num_robots);
  pubviz_r_map_.resize(num_robots);
  pubviz_r_traj_.resize(num_robots);
  pubviz_r_corres_.resize(num_robots);
  pubviz_r_transform_.resize(num_robots);
  r_landmarks_.resize(num_robots);
  r_traj_.resize(num_robots);

  // initialize
  r_first_data_.resize(num_robots, false);
  std::fill(r_first_data_.begin(), r_first_data_.end(), false);
  
 
  // set initial relative transform to something arbitrary (for visualization)
  T_R1iRii_.resize(num_robots);
  T_R1iRii_[0].setIdentity();
  for (size_t i=1; i<num_robots; ++i)
  {
    T_R1iRii_[i].setIdentity();
    T_R1iRii_[i].translation() = Eigen::Vector3d(i, 0, 0);
    Eigen::Quaterniond q;
    q = Eigen::AngleAxisd(0, Eigen::Vector3d::UnitZ());
    T_R1iRii_[i].linear() = q.toRotationMatrix();
  }
  

  // CLIPPER setup
  clipper::Params params;
  clipper::invariants::EuclideanDistance::Params iparams;
  iparams.sigma = clipper_sigma; // sigma for the Guassian weighting
  iparams.epsilon = clipper_epsilon; // maximum error to consider two associations consistent
  iparams.mindist = clipper_mindist; // minimum distance between chosen objects in same map
  clipper_.reset(new clipper::CLIPPER<clipper::invariants::EuclideanDistance>(params, iparams));


  for (size_t i=0; i<num_robots; ++i)
  {
    boost::function<void(const sensor_msgs::PointCloud2ConstPtr&)> cb1 =
      [=](const sensor_msgs::PointCloud2ConstPtr& msg) {landmarks_cb(msg, i);};

    sub_r_landmarks_[i] = nh_.subscribe(std::string("/") + robots[i] + "/landmarks", 1, cb1);

    boost::function<void(const nav_msgs::PathConstPtr&)> cb2 =
      [=](const nav_msgs::PathConstPtr& msg) {traj_cb(msg, i);};

    sub_r_traj_[i] = nh_.subscribe(std::string("/") + robots[i] + trajectory_topics_[i], 1, cb2);

    pubviz_r_frame_[i] = nh_.advertise<geometry_msgs::PoseStamped>(
        std::string("/clipper/") + robots[i] + "/frame", 1);

    pubviz_r_traj_[i] = nh_.advertise<nav_msgs::Path> (
      std::string("/clipper/") + robots[i] + "/trajectory", 1);

    pubviz_r_map_[i] = nh_.advertise<sensor_msgs::PointCloud2>(
        std::string("/clipper/") + robots[i] + "/landmarks", 1);

    pubviz_r_transform_[i] = nh_.advertise<geometry_msgs::Pose>(
        std::string("/clipper/") + robots[i] + "/transform", 1);
  }

  // correspondence output, numbered as 2,...,num_robots
  for (size_t i=1; i<num_robots; ++i)
  {
    pubviz_r_corres_[i] = nh_.advertise<visualization_msgs::MarkerArray>(
      "/clipper/correspondences"+std::to_string(i+1), 1);
  }

  // main loop timer for viz and performing alignments
  nhp_.param<double>("aligner_dt", aligner_dt_, 3);
  tim_aligner_ = nh_.createTimer(ros::Duration(aligner_dt_), &Aligner::timer_cb, this);


  ROS_INFO_STREAM("Aligner initialized");
}

// ----------------------------------------------------------------------------
// Private Methods
// ----------------------------------------------------------------------------
void Aligner::align_maps(uint i) 
{
  ROS_INFO_STREAM("Aligning maps...");

  // nothing to do if maps aren't big enough to estimate (R,t)
  if (r_landmarks_[0].width < N || r_landmarks_[i].width < N) 
  {
    ROS_INFO_STREAM("CLIPPER aborted: too few landmarks between robots 0 and " << i << ".  ");
    return;
  }

  pcl::PointCloud<pcl::PointXYZ> r1_cloud, ri_cloud;
  pcl::fromROSMsg(r_landmarks_[0], r1_cloud);
  pcl::fromROSMsg(r_landmarks_[i], ri_cloud);

  Eigen::MatrixXd model = r1_cloud.getMatrixXfMap().topRows(DIM).cast<double>();
  Eigen::MatrixXd data = ri_cloud.getMatrixXfMap().topRows(DIM).cast<double>();

  // identify data assocation
  const auto t1 = std::chrono::high_resolution_clock::now();
  Ain_ = clipper_->findCorrespondences(model, data);
  const auto t2 = std::chrono::high_resolution_clock::now();

  const auto duration = std::chrono::duration_cast<std::chrono::nanoseconds>(t2 - t1);
  const double elapsed_ms = static_cast<double>(duration.count()) * 1e-6;

  const int m = model.cols() * data.cols(); // assumes all-to-all hypothesis
  ROS_INFO_STREAM("CLIPPER found " << Ain_.rows() << " out of " << m
    << " associations in " << std::round(elapsed_ms) << " ms  ");

  // we needed at least N correspondences to estimate (R,t)
  if (Ain_.rows() < N) return;  
  // if ((Ain_.rows() < N) || (Ain_.rows() < num_match)) {
  //    return; } else { num_match = Ain_.rows(); }

  // rearrange data so that cols correspRond
  Eigen::Matrix3Xd p = Eigen::Matrix3Xd(3, Ain_.rows());
  Eigen::Matrix3Xd q = Eigen::Matrix3Xd(3, Ain_.rows());
  for (size_t i=0; i<Ain_.rows(); ++i) {
    p.col(i) = model.col(Ain_(i,0));
    q.col(i) = data.col(Ain_(i,1));
  }
  // ROS_WARN_STREAM("p: " << p);
  // ROS_WARN_STREAM("q: " << q);

  // find transformation that registers q onto p
  Eigen::Matrix4d T_pq = Eigen::umeyama(q, p, false);

  // update our estimate
  T_R1iRii_[i].translation() = T_pq.block<3,1>(0,3);
  T_R1iRii_[i].linear() = T_pq.block<3,3>(0,0);

  ROS_INFO_STREAM("transform:\n " << T_R1iRii_[i].matrix());

  // transform recovered transform to ROS pose and publish
  geometry_msgs::Pose msg;
  // msg.header.frame_id = robots[0] + "/" + map_frame_id_;
  tf::poseEigenToMsg(T_R1iRii_[i], msg);            
  pubviz_r_transform_[i].publish(msg);

}



// ----------------------------------------------------------------------------
// ROS Callbacks
// ----------------------------------------------------------------------------

void Aligner::landmarks_cb(const sensor_msgs::PointCloud2ConstPtr& msg, const uint i)
{
  // ROS_WARN_STREAM("i: " << i << ", msg->width: " << msg->width);
  // ROS_WARN_STREAM("i: " << i << ", r_landmarks_[i].width: " << r_landmarks_[i].width);
  
  if (!r_first_data_[i]) {    
    r_first_data_[i] = (msg->width > 0); // has received first data
  }

  // landmarks of robot i
  r_landmarks_[i] = *msg;  
  r_landmarks_[i].header.stamp = ros::Time::now(); // reset time to now
  
  // add a frame_id if missing
  if (r_landmarks_[i].header.frame_id.empty()) {
    r_landmarks_[i].header.frame_id = robots[i] + "/" + map_frame_id_;
  }
  
  // ROS_WARN_STREAM("i\: " << i << ", r_landmarks_[i].width: " << r_landmarks_[i].width);
}


// ----------------------------------------------------------------------------
void Aligner::traj_cb(const nav_msgs::PathConstPtr& msg, const uint i)
{
  // ROS_INFO_STREAM("getting traj for robot " << i << ".  ");

  // get robot's trajectory
	r_traj_[i] = *msg; // robot trajectory

}


// ----------------------------------------------------------------------------
 // transofrm trajecotry basaed on affine trasform T
void Aligner::transform_traj(const Eigen::Affine3d& T, 
                             const nav_msgs::Path& traj, 
                             nav_msgs::Path& traj_new)
{
  // copy trajectory
  traj_new = traj;
  traj_new.header.stamp = ros::Time::now();

  const double traj_length = traj.poses.size();
  // transform robot's trajectory
  for (int i=0; i<traj_length; ++i)
  {
    const geometry_msgs::Pose& pose = traj.poses[i].pose;
    Eigen::Affine3d pose_eigen;

    tf::poseMsgToEigen(pose, pose_eigen); // transform to Eigen

    Eigen::Affine3d pose_eigen_new;
    pose_eigen_new = T * pose_eigen; // apply transform 

    geometry_msgs::Pose pose_new;
    tf::poseEigenToMsg(pose_eigen_new, pose_new); // transform to Pose msg

    traj_new.poses[i].pose = pose_new;
    // traj_new.poses[i].header.stamp = ros::Time::now();
  }
	
}


// ----------------------------------------------------------------------------
void Aligner::timer_cb(const ros::TimerEvent& event)
{
  // ROS_INFO_STREAM("CLIPPER timer callback  ");

  // if we have initial data, we can do alignment
  if (r_first_data_[0])
  {
    // publish robot 1 start frame as identity
    geometry_msgs::PoseStamped framemsg;
    framemsg.header.stamp = ros::Time::now();
    framemsg.header.frame_id = robots[0] + "/" + map_frame_id_;
    framemsg.pose = tf2::toMsg(T_R1iRii_[0]);
    pubviz_r_frame_[0].publish(framemsg);

    // publish Robot 1 Map (w.r.t Robot1 initial frame, which is chosen as world)
    if (!r_landmarks_[0].header.frame_id.empty()) 
    {
      sensor_msgs::PointCloud2 msg = r_landmarks_[0];
      msg.header.frame_id = robots[0] + "/" + map_frame_id_;       
      pubviz_r_map_[0].publish(msg);

      nav_msgs::Path msg2 = r_traj_[0];
      msg2.header.stamp = ros::Time::now();
      msg2.header.frame_id = robots[0] + "/" + map_frame_id_;
      pubviz_r_traj_[0].publish(msg2);
    }

    // Publish Robot i Map (w.r.t Robot1 initial frame)
    for (size_t i=1; i<num_robots; ++i)
    {
      // don't do anything until received initial robot pose
      if (!r_first_data_[i]) continue;

      // find relative transform between robots 1 and i using CLIPPER
      Aligner::align_maps(i); 


      // publish robot i start frame w.r.t robot 1 start frame (to be estimated)
      geometry_msgs::PoseStamped framemsg;
      framemsg.header.stamp = ros::Time::now();
      framemsg.header.frame_id = robots[0] + "/" + map_frame_id_;
      framemsg.pose = tf2::toMsg(T_R1iRii_[i]);
      pubviz_r_frame_[i].publish(framemsg);

      if (!r_landmarks_[i].header.frame_id.empty()) 
      {
        // Transform {landmarks w.r.t Rii} to {landmarks w.r.t R1i}
        sensor_msgs::PointCloud2 msg;
        pcl_ros::transformPointCloud(T_R1iRii_[i].matrix().cast<float>(), r_landmarks_[i], msg);
        msg.header.frame_id = robots[0] + "/" + map_frame_id_;       
        pubviz_r_map_[i].publish(msg);

        // transform trajectory to world frame
        nav_msgs::Path msg2;
        Aligner::transform_traj(T_R1iRii_[i], r_traj_[i], msg2);
        msg2.header.frame_id = robots[0] + "/" + map_frame_id_;
        pubviz_r_traj_[i].publish(msg2);
      }


      // Publish Correspondences
      if (Ain_.rows() >= N && !r_landmarks_[i].header.frame_id.empty()) { 
        visualization_msgs::MarkerArray vizmsg;

        // Transform {landmarks w.r.t Rii} to {landmarks w.r.t R1i}
        pcl::PointCloud<pcl::PointXYZ> r1_cloud, ri_cloud, ri_aligned_cloud;
        pcl::fromROSMsg(r_landmarks_[0], r1_cloud);
        pcl::fromROSMsg(r_landmarks_[i], ri_cloud);
        pcl::transformPointCloud(ri_cloud, ri_aligned_cloud, T_R1iRii_[i].cast<float>());

        for (size_t j=0; j<Ain_.rows(); j++) {
          visualization_msgs::Marker m;
          m.header.stamp = ros::Time::now();
          m.header.frame_id = robots[0] + "/" + map_frame_id_;
          m.id = j;
          m.type = visualization_msgs::Marker::LINE_STRIP;
          m.action = visualization_msgs::Marker::ADD;
          m.color.r = 0;
          m.color.g = 1;
          m.color.b = 0;
          m.color.a = 1;
          m.scale.x = 0.05;
          m.pose.orientation.w = 1;
          // gross
          m.points.push_back(tf2::toMsg(static_cast<Eigen::Vector3d>(
                  r1_cloud.getMatrixXfMap().topRows(3).cast<double>().col(Ain_(j,0)))));
          m.points.push_back(tf2::toMsg(static_cast<Eigen::Vector3d>(
                  ri_aligned_cloud.getMatrixXfMap().topRows(3).cast<double>().col(Ain_(j,1)))));
          vizmsg.markers.push_back(m);
        }
        pubviz_r_corres_[i].publish(vizmsg);
      }
    }
  }

  // Publish out TF information -- before alignment, all tfs will be identity
  std::vector<geometry_msgs::TransformStamped> tfs;

  // broadcase current tf frames
  for (size_t i=0; i<num_robots; ++i) {
    geometry_msgs::TransformStamped tfmsg = tf2::eigenToTransform(T_R1iRii_[i]);
    tfmsg.header.stamp = ros::Time::now();
    tfmsg.header.frame_id = world_frame_id_;
    tfmsg.child_frame_id = robots[i] + "/" + map_frame_id_;
    tfs.push_back(tfmsg);
  } 

  tfb_.sendTransform(tfs);
}


} // ns lcd



// 
// In header
// struct EIGEN_ALIGN16 PointXYZRGBLabel {
//   float x;
//   float y;
//   float z;
//   float rgba;
//   uint32_t label;
//   EIGEN_MAKE_ALIGNED_OPERATOR_NEW

//   static inline PointXYZRGBLabel make(float x, float y, float z, float rgba, uint32_t label) {
//     return {x, y, z, rgba, label};
//   }
// };

// // clang-format off
// POINT_CLOUD_REGISTER_POINT_STRUCT(PointXYZRGBLabel,
//   (float, x, x)
//   (float, y, y)
//   (float, z, z)
//   (float, rgba, rgba)
//   (uint32_t, label, label)
// )

// // in callback
// void ns::pcCb(const sensor_msgs::PointCloud2::ConstPtr &pc_msg) {
//   pcl::PointCloud<PointXYZRGBLabel> cloud{};
//   pcl::fromROSMsg(*pc_msg, cloud);
  
//   for (size_t, i=0; i<cloud.points.size(); i++) {
//     cloud.points[i].x;
//     cloud.points[i].y;
//     cloud.points[i].z;
//     cloud.points[i].rgba;
//     cloud.points[i].label;
//   }
// }